module.exports = {
  extends: ['airbnb'],
  rules: {
    'react/jsx-filename-extension': 0,
    'import/no-extraneous-dependencies': 0,
    'react/jsx-one-expression-per-line': 0,
  },
  parser: 'babel-eslint',
  env: {
    browser: 1,
  },
};

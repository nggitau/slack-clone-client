import React from 'react';
import { string, shape } from 'prop-types';
import decode from 'jwt-decode';
import findIndex from 'lodash.findindex';
import { graphql } from 'react-apollo';

import Channels from '../components/Channels';
import Teams from '../components/Teams';
import AddChannelModal from '../components/AddChannelModal';
import { allTeamsQuery } from '../graphql/team';

class Sidebar extends React.Component {
  state = {
    openAddChannelModal: false,
  };

  handleAddChannelClick = () => {
    this.setState({ openAddChannelModal: true });
  };

  handleCloseAddChannelModal = () => {
    this.setState({ openAddChannelModal: false });
  };

  render() {
    const {
      data: { loading, allTeams },
      currentTeamId,
    } = this.props;
    const { openAddChannelModal } = this.state;
    if (loading) return null;

    const teamIdx = currentTeamId ? findIndex(allTeams, ['id', parseInt(currentTeamId, 10)]) : 0;
    const team = allTeams[teamIdx];

    let username = '';
    try {
      const token = localStorage.getItem('x-access-token');
      const { user } = decode(token);
      // eslint-disable-next-line prefer-destructuring
      username = user.username;
    } catch (error) {} // eslint-disable-line no-empty

    return (
      <React.Fragment>
        <Teams
          teams={allTeams.map(t => ({
            id: t.id,
            letter: t.name.charAt(0).toUpperCase(),
          }))}
        />
        <Channels
          teamName={team.name}
          username={username}
          teamId={team.id}
          channels={team.channels}
          users={[{ id: 1, name: 'slackbot' }, { id: 2, name: 'user1' }]}
          onAddChannelClick={this.handleAddChannelClick}
        />
        <AddChannelModal
          teamId={currentTeamId}
          open={openAddChannelModal}
          onClose={this.handleCloseAddChannelModal}
          key="sidebar-add-channel-modal"
        />
      </React.Fragment>
    );
  }
}

Sidebar.propTypes = {
  data: shape({}).isRequired,
  currentTeamId: string.isRequired,
};

export default graphql(allTeamsQuery)(Sidebar);

import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

const GET_USERS = gql`
  {
    allUsers {
      id
      email
    }
  }
`;

const Home = () => (
  <Query query={GET_USERS}>
    {({ loading, error, data }) => {
      if (loading) {
        return (
          <p>
            {}
            Loading...
          </p>
        );
      }

      if (error) {
        return (
          <p>
            {`Error! ${error.message}`}
          </p>
        );
      }

      return data.allUsers.map(user => (
        <div key={user.id}>
          <h1>
            {user.email}
          </h1>
        </div>
      ));
    }}
  </Query>
);

export default Home;

import React, { Component } from 'react';
import { extendObservable } from 'mobx';
import { observer } from 'mobx-react';
import {
  Button, Container, Form, Header, Input, Message,
} from 'semantic-ui-react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const createTeamMutation = gql`
  mutation($name: String!) {
    createTeam(name: $name) {
      ok
      team {
        id
      }
      errors {
        path
        message
      }
    }
  }
`;

class CreateTeam extends Component {
  constructor(props) {
    super(props);

    extendObservable(this, {
      name: '',
      errors: {},
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this[name] = value;
  };

  // eslint-disable-next-line consistent-return
  onSubmit = async (e) => {
    e.preventDefault();
    const { name } = this;
    const { history, mutate } = this.props;
    let response = null;

    try {
      response = await mutate({
        variables: { name },
      });
    } catch (error) {
      return history.push('/login');
    }

    const {
      ok, team, errors,
    } = response.data.createTeam;

    if (ok) {
      history.push(`/view-team/${team.id}`);
    } else {
      const err = {};
      errors.forEach(({ path, message }) => {
        err[`${path}Error`] = message;
      });
      this.errors = err;
    }
  };

  render() {
    const {
      name,
      errors: { nameError },
    } = this;

    const errorList = [];

    if (nameError) errorList.push(nameError);

    return (
      <Container text>
        <Header as="h2">
          CreateTeam
        </Header>
        <Form>
          <Form.Field error={!!nameError}>
            <Input
              fluid
              name="name"
              onChange={this.onChange}
              placeholder="Name"
              type="email"
              value={name}
            />
          </Form.Field>
          <Button onClick={this.onSubmit}>
            Submit
          </Button>
        </Form>
        {errorList.length ? (
          <Message error header="There was some errors with your submission" list={errorList} />
        ) : null}
      </Container>
    );
  }
}

export default graphql(createTeamMutation)(observer(CreateTeam));

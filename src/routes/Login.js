import React, { Component } from 'react';
import { extendObservable } from 'mobx';
import { observer } from 'mobx-react';
import {
  Button, Container, Form, Header, Input, Message,
} from 'semantic-ui-react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const loginMutation = gql`
  mutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      ok
      token
      refreshToken
      errors {
        path
        message
      }
    }
  }
`;
class Login extends Component {
  constructor(props) {
    super(props);

    extendObservable(this, {
      email: '',
      password: '',
      errors: {},
    });
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this[name] = value;
  };

  onSubmit = async (e) => {
    e.preventDefault();
    const { email, password } = this;
    const { history, mutate } = this.props;
    const response = await mutate({
      variables: { email, password },
    });
    const {
      ok, token, refreshToken, errors,
    } = response.data.login;

    if (ok) {
      localStorage.setItem('x-access-token', token);
      localStorage.setItem('x-refresh-token', refreshToken);
      history.push('/');
    } else {
      const err = {};
      errors.forEach(({ path, message }) => {
        err[`${path}Error`] = message;
      });
      this.errors = err;
    }
  };

  render() {
    const {
      email,
      password,
      errors: { emailError, passwordError },
    } = this;

    const errorList = [];

    if (emailError) errorList.push(emailError);
    if (passwordError) errorList.push(passwordError);

    return (
      <Container text>
        <Header as="h2">
          Login
        </Header>
        <Form>
          <Form.Field error={!!emailError}>
            <Input
              fluid
              name="email"
              onChange={this.onChange}
              placeholder="Email"
              type="email"
              value={email}
            />
          </Form.Field>
          <Form.Field error={!!passwordError}>
            <Input
              fluid
              name="password"
              onChange={this.onChange}
              placeholder="Password"
              type="password"
              value={password}
            />
          </Form.Field>
          <Button onClick={this.onSubmit}>
            Submit
          </Button>
        </Form>
        {errorList.length ? (
          <Message error header="There was some errors with your submission" list={errorList} />
        ) : null}
      </Container>
    );
  }
}

export default graphql(loginMutation)(observer(Login));

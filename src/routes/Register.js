import React, { Component } from 'react';
import {
  Button, Container, Form, Header, Input, Message,
} from 'semantic-ui-react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

const registerMutation = gql`
  mutation register($username: String!, $email: String!, $password: String!) {
    register(username: $username, email: $email, password: $password) {
      ok
      errors {
        path
        message
      }
    }
  }
`;

class Register extends Component {
  state = {
    username: '',
    usernameError: '',
    email: '',
    emailError: '',
    password: '',
    passwordError: '',
  };

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  onSubmit = async (e) => {
    e.preventDefault();
    this.setState({
      usernameError: '',
      emailError: '',
      passwordError: '',
    });

    const { username, email, password } = this.state;
    const { mutate, history } = this.props;

    const response = await mutate({ variables: { username, email, password } });

    const { ok, errors } = response.data.register;

    if (ok) {
      history.push('/');
    } else {
      const err = {};
      errors.forEach(({ path, message }) => {
        err[`${path}Error`] = message;
      });
      this.setState(err);
    }
  };

  render() {
    const {
      username, email, password, usernameError, emailError, passwordError,
    } = this.state;

    const errorList = [];

    if (usernameError) errorList.push(usernameError);
    if (emailError) errorList.push(emailError);
    if (passwordError) errorList.push(passwordError);

    return (
      <Container text>
        <Header as="h2">
          Register
        </Header>
        <Form>
          <Form.Field error={!!usernameError}>
            <Input
              fluid
              name="username"
              onChange={this.onChange}
              placeholder="Username"
              value={username}
            />
          </Form.Field>
          <Form.Field error={!!emailError}>
            <Input
              fluid
              name="email"
              onChange={this.onChange}
              placeholder="Email"
              type="email"
              value={email}
            />
          </Form.Field>
          <Form.Field error={!!passwordError}>
            <Input
              fluid
              name="password"
              onChange={this.onChange}
              placeholder="Password"
              type="password"
              value={password}
            />
          </Form.Field>
          <Button onClick={this.onSubmit}>
            Submit
          </Button>
        </Form>
        {errorList.length ? (
          <Message error header="There was some errors with your submission" list={errorList} />
        ) : null}
      </Container>
    );
  }
}

export default graphql(registerMutation)(Register);
